import abc

from tbot.machine import linux


class LabBase(linux.Lab):
    """
    Base-Class for the different developers' lab-hosts.

    This base-class defines common 'interfaces' (i.e. methods and attributes)
    that each lab-host should implement.
    """

    # Example for an 'optional' method.  Not implementing this one could lead
    # to runtime errors when calling testcases that want to use it.
    def source_dir(self):
        raise NotImplementedError(f"{self!r} does not implement lh.source_dir()")

    # Example for a 'mandatory' method.  You cannot start any testcases when
    # this method isn't configured properly for a lab.
    @abc.abstractmethod
    def enable_tftp_server(self):
        pass

    # Example for an optional attribute.  Either give it a default value here
    # of set it to `None` to indicate a missing value.
    server_hostname = "localhost"

    # Example for a 'mandatory' attribute.  This attribute needs to be
    # configured for a lab-config to be usable.
    #
    # While it needs to look like a method here, it is configured as a normal
    # attribute in the lab-config:
    #
    #   ipaddr = "192.0.2.1"
    @property
    @abc.abstractmethod
    def ipaddr(self):
        pass

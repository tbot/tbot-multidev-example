from tbot.machine import connector, linux, board

import lab_base
import board_base


class HwsLab(connector.SubprocessConnector, linux.Bash, lab_base.LabBase):
    name = "hws-lab"

    # Configuration of the common interfaces defined in the lab-base class:

    def source_dir(self):
        return self.fsroot / "/home/hws/Documents/Development/tbot"

    def enable_tftp_server(self):
        self.exec0("sudo", "systemctl", "start", "tftp.socket")

    server_hostname = "hws-lab"
    ipaddr = "192.0.2.1"


class HwsBoard(connector.ConsoleConnector, board.PowerControl, board.Board):
    name = "board-hws"

    def poweron(self):
        self.host.exec0("power-control.sh", self.name, "on")

    def poweroff(self):
        self.host.exec0("power-control.sh", self.name, "off")

    def connect(self, mach):
        return mach.open_channel("picocom", "-q", "-b", str(115200), "/dev/ttyUSB1")


LAB = HwsLab
BOARD = HwsBoard
UBOOT = board_base.BoardUBoot
LINUX = board_base.BoardLinux

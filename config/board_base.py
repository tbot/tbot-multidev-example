from tbot.machine import board, linux


class BoardUBoot(board.Connector, board.UBootAutobootIntercept, board.UBootShell):
    """
    Default configuration for the Board's U-Boot machine
    """

    name = "board-uboot"
    prompt = "=> "


class BoardLinux(board.LinuxUbootConnector, board.LinuxBootLogin, linux.Ash):
    """
    Default configuration for the Board's Linux machine
    """

    name = "board-linux"

    uboot = BoardUBoot

    def do_boot(self, ub):
        return ub.boot("run", "bootcmd")

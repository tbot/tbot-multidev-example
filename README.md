tbot example repository
=======================
This repository lays out an example directory structure which could be
used for working with tbot.  It is tuned to the use-case where multiple
developers work on the same project and want to reuse the testcases.

## Overview
To allow writing generic testcases that work for every developer, even
when their setups differ vastly, all 'lab'-specific logic has to be
extracted into the config.

Testcases can call into this logic via common interfaces defined in
`config/lab_base.py`.  The developer-specific lab-config then implements
these interfaces.

Each developer has a file like `config/hws.py` which contains the
lab-config and lab-specific board-config.  The Board's software (U-Boot
& Linux) is the same for everyone in most cases so it is only defined once
in `config/board_base.py`.

Additionally, each developer has an args file like `args/hws` for ease of
use.  This file contains their personal tbot command-line arguments that
are passed on each invocation.  One line is one separate arg.  This file
passes the dev's config as both lab- and board-config (see `args/hws` for
an example).

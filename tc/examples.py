import tbot


@tbot.testcase
@tbot.with_lab
def test_foo_bar(lh):
    # In a testcase, you can now make use of the common interfaces from `lab_base.py`:
    lh.enable_tftp_server()

    path = lh.source_dir()
    assert path.exists()

    tbot.log.message(f"Host {lh.server_hostname} has ip {lh.ipaddr}")
